package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.CityEvent;
import com.lukaszjok.mturek.backend.ItemObjects.Movie;

import java.util.ArrayList;


/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapteCityEventsList extends BaseAdapter {
    private Context ctx;
    private ArrayList<CityEvent> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvCityEventsName;
        public TextView tvCityEventsDescriprion;
        public TextView tvCityEventsPlace;
        public TextView tvCityEventsDate;
        public TextView tvCityEventsTime;
        public TextView tvCityEventsPrice;
        public TextView tvCityEventsAgeLimit;
    }

    public AdapteCityEventsList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final CityEvent item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public CityEvent getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<CityEvent>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.city_event_item, parent, false);

            holder = new ViewHolder();
            holder.tvCityEventsName = (TextView) convertView.findViewById(R.id.tv_city_event_item_name);
            holder.tvCityEventsDescriprion = (TextView) convertView.findViewById(R.id.tv_city_event_item_description);
            holder.tvCityEventsPlace = (TextView) convertView.findViewById(R.id.tv_city_event_item_place);
            holder.tvCityEventsDate = (TextView) convertView.findViewById(R.id.tv_city_event_item_date);
            holder.tvCityEventsTime = (TextView) convertView.findViewById(R.id.tv_city_event_item_time);
            holder.tvCityEventsPrice = (TextView) convertView.findViewById(R.id.tv_city_event_item_price);
            holder.tvCityEventsAgeLimit = (TextView) convertView.findViewById(R.id.tv_city_event_age_limit);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvCityEventsName.setText(mData.get(position).getName().toUpperCase());
        holder.tvCityEventsDescriprion.setText(mData.get(position).getDescription());
        holder.tvCityEventsPlace.setText(mData.get(position).getPlace());
        holder.tvCityEventsDate.setText(mData.get(position).getDate());
        holder.tvCityEventsTime.setText(mData.get(position).getTime());
        holder.tvCityEventsPrice.setText(mData.get(position).getPrice());
        holder.tvCityEventsAgeLimit.setText(mData.get(position).getAge_limit());

        return convertView;
    }




}

