package com.lukaszjok.mturek.backend.ItemObjects;

public class AndroidLicense {
    private String libraryName;    private String description;


    public AndroidLicense(){
        this.libraryName = "";
        this.description = "";
    }

    public AndroidLicense(String libraryName, String description){
        this.libraryName = libraryName;
        this.description = description;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
