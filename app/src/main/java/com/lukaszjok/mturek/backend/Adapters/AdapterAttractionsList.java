package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.Attraction;

import java.util.ArrayList;

/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterAttractionsList extends BaseAdapter {
    private Context ctx;
    private ArrayList<Attraction> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvAttractionName;
        public TextView tvAttractionDescriptionShort;
}

    public AdapterAttractionsList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final Attraction item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Attraction getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<Attraction>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.attraction_item, parent, false);

            holder = new ViewHolder();
            holder.tvAttractionName = (TextView) convertView.findViewById(R.id.tv_attraction_name);
            holder.tvAttractionDescriptionShort = (TextView) convertView.findViewById(R.id.tv_attraction_desription_short);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvAttractionName.setText(mData.get(position).getName().toUpperCase());
        holder.tvAttractionDescriptionShort.setText(mData.get(position).getDescription_short().replaceAll("\\\\n", "\n"));


        return convertView;
    }




}

