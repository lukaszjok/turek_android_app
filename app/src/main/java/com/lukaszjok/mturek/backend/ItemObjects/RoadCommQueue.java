package com.lukaszjok.mturek.backend.ItemObjects;

public class RoadCommQueue {
    private String dealName;
    private String numOfTicketsInQueue;
    private String currentTicket;
    private String lastTicket;

    public RoadCommQueue(){
        this.dealName = "";
        this.numOfTicketsInQueue = "";
        this.currentTicket = "";
        this.lastTicket = "";
    }

    public RoadCommQueue(String dealName,String numOfTicketsInQueue, String currentTicket, String lastTicket){
        this.dealName = dealName;
        this.numOfTicketsInQueue = numOfTicketsInQueue;
        this.currentTicket = currentTicket;
        this.lastTicket = lastTicket;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public String getNumOfTicketsInQueue() {
        return numOfTicketsInQueue;
    }

    public void setNumOfTicketsInQueue(String numOfTicketsInQueue) {
        this.numOfTicketsInQueue = numOfTicketsInQueue;
    }

    public String getCurrentTicket() {
        return currentTicket;
    }

    public void setCurrentTicket(String currentTicket) {
        this.currentTicket = currentTicket;
    }

    public String getLastTicket() {
        return lastTicket;
    }

    public void setLastTicket(String lastTicket) {
        this.lastTicket = lastTicket;
    }
}
