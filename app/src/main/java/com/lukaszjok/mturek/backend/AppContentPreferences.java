package com.lukaszjok.mturek.backend;

import android.content.Context;
import android.content.SharedPreferences;


public class AppContentPreferences {
    SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public Context context;
    int PRIVATE_MODE = 0;


    public static final String PREF_NAME = "APP_CONTENT_PREFERENCES";
    public static final String INTERVENTION_DESCRIPTION_KEY = "INTERVENTION_DESCRIPTION";
    public static final String INTERVENTION_EMAIL_KEY = "INTERVENTION_EMAIL";
    public static final String INTERVENTION_CALL_NUMBER = "INTERVENTION_CALL_NUMBER";
    public static final String MAIN_ALERT = "MAIN_ALERT";
    public static final String IS_MAIN_ALERT_VISSABLE = "IS_MAIN_ALERT_VISSABLE";
    public static final String APP_CONTACT_MAIL = "APP_CONTACT_MAIL";

    public static final String CUST_AD_IS_VISABLE = "CUST_AD_IS_VISABLE";
    public static final String CUST_AD_TITLE = "CUST_AD_TITLE";
    public static final String CUST_AD_MESSAGE = "CUST_AD_MESSAGE";
    public static final String CUST_AD_IMAGE_LINK = "CUST_AD_IMAGE_LINK";
    public static final String CUST_AD_IMAGE_WIDTH = "CUST_AD_IMAGE_WIDTH";
    public static final String CUST_AD_IMAGE_HEIGHT = "CUST_AD_IMAGE_HEIGHT";

    public static final String ARE_TEST_ADS = "ARE_TEST_ADS";

    public AppContentPreferences(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public String getInterventionDescription(){
        return sharedPreferences.getString(INTERVENTION_DESCRIPTION_KEY,"");
    }
    public void setInterventionDescription(String desc){
        editor.putString(INTERVENTION_DESCRIPTION_KEY, desc);
        editor.apply();
    }

    public String getInterventionEmail(){
        return sharedPreferences.getString(INTERVENTION_EMAIL_KEY,"");
    }
    public void setInterventionEmail(String email){
        editor.putString(INTERVENTION_EMAIL_KEY,email);
        editor.apply();
    }

    public String getInterventionCallNumber(){
        return sharedPreferences.getString(INTERVENTION_CALL_NUMBER,"");
    }
    public void setInterventionCallNumber(String callNumber){
        editor.putString(INTERVENTION_CALL_NUMBER,callNumber);
        editor.apply();
    }

    public String getMainAlert(){
        return sharedPreferences.getString(MAIN_ALERT,"");
    }
    public void setMainAlert(String alert){
        editor.putString(MAIN_ALERT,alert);
        editor.apply();
    }

    public String getIsMainAlertVissable(){
        return sharedPreferences.getString(IS_MAIN_ALERT_VISSABLE,"");
    }
    public void setIsMainAlertVissable(String flag){
        editor.putString(IS_MAIN_ALERT_VISSABLE,flag);
        editor.apply();
    }
    public String getAppContactMail(){
        return sharedPreferences.getString(APP_CONTACT_MAIL,"");
    }
    public void setAppContactMail(String mail){
        editor.putString(APP_CONTACT_MAIL,mail);
        editor.apply();
    }

    //////

    public String getCustAdIsVisable(){
        return sharedPreferences.getString(CUST_AD_IS_VISABLE,"");
    }
    public void setCustAdIsVisable(String flag){
        editor.putString(CUST_AD_IS_VISABLE,flag);
        editor.apply();
    }

    public String getCustAdTitle(){
        return sharedPreferences.getString(CUST_AD_TITLE,"");
    }
    public void setCustAdTitle(String title){
        editor.putString(CUST_AD_TITLE,title);
        editor.apply();
    }

    public String getCustAdMessage(){
        return sharedPreferences.getString(CUST_AD_MESSAGE,"");
    }
    public void setCustAdMessage(String message){
        editor.putString(CUST_AD_MESSAGE,message);
        editor.apply();
    }

    public String getCustAdImageLink(){
        return sharedPreferences.getString(CUST_AD_IMAGE_LINK,"");
    }
    public void setCustAdImageLink(String imageLink){
        editor.putString(CUST_AD_IMAGE_LINK,imageLink);
        editor.apply();
    }

    public String getCustAdImageWidth(){
        return sharedPreferences.getString(CUST_AD_IMAGE_WIDTH,"0");
    }
    public void setCustAdImageWidth(String width){
        editor.putString(CUST_AD_IMAGE_WIDTH,width);
        editor.apply();
    }

    public String getCustAdImageHeight(){
        return sharedPreferences.getString(CUST_AD_IMAGE_HEIGHT,"0");
    }
    public void setCustAdImageHeight(String height){
        editor.putString(CUST_AD_IMAGE_HEIGHT,height);
        editor.apply();
    }

    public String getAreTestAds(){
        return sharedPreferences.getString(ARE_TEST_ADS,"0");
    }
    public void setAreTestAds(String state){
        editor.putString(ARE_TEST_ADS,state);
        editor.apply();
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }




}
