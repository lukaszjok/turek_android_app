package com.lukaszjok.mturek.backend.ItemObjects;

public class School {
    private int id;
    private String typeName;
    private String name;
    private String address;
    private String phone;
    private String website;

    public School() {
        this.id = -1 ;
        this.typeName = "";
        this.name = "";
        this.address = "";
        this.phone = "";
        this.website = "";
    }

    public School(int id, String name, String typeName, String address, String phone, String website) {
        this.id = id;
        this.typeName = typeName;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.website = website;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
