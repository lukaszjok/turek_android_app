package com.lukaszjok.mturek.backend.ItemObjects;

public class Restaurant {
    private int idRestaurant;
    private String name;
    private String descriptionShort;
    private String phone;
    private String minOrderCost;
    private String workTime;
    private String address;

    public Restaurant(){
        this.idRestaurant = -1;
        this.name = "";
        this.descriptionShort = "";
        this.phone = "";
        this.minOrderCost = "";
        this.workTime = "";
        this.address = "";
    }

    public Restaurant(int idRestaurant, String name, String descriptionShort, String phone, String minOrderCost,
                      String workTime, String address){
        this.idRestaurant = idRestaurant;
        this.name = name;
        this.descriptionShort = descriptionShort;
        this.phone = phone;
        this.minOrderCost = minOrderCost;
        this.workTime = workTime;
        this.address = address;
    }

    public int getId() {
        return idRestaurant;
    }

    public void setId(int id) {
        this.idRestaurant = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMinOrderCost() {
        return minOrderCost;
    }

    public void setMinOrderCost(String minOrderCost) {
        this.minOrderCost = minOrderCost;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
