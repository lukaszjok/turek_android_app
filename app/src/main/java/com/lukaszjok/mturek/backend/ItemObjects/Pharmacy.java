package com.lukaszjok.mturek.backend.ItemObjects;

public class Pharmacy {
    private String name;
    private String phone;
    private String workTime;
    private String address;

    public Pharmacy(){
        this.name = "";
        this.phone = "";
        this.workTime = "";
        this.address = "";
    }

    public Pharmacy(String name, String phone, String workTime, String address){
        this.name = name;
        this.phone = phone;
        this.workTime = workTime;
        this.address = address;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}