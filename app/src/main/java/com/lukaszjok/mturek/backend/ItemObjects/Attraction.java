package com.lukaszjok.mturek.backend.ItemObjects;

public class Attraction {
    private int id;
    private String name;
    private String description_short;

    public Attraction(){
        this.id = -1;
        this.name = "";
        this.description_short = "";
    }

    public Attraction(int id, String name, String description_short){
        this.id = id;
        this.name = name;
        this.description_short = description_short;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription_short() {
        return description_short;
    }

    public void setDescription_short(String description_short) {
        this.description_short = description_short;
    }
}
