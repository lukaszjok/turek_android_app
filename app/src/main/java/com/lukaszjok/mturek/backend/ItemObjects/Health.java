package com.lukaszjok.mturek.backend.ItemObjects;

public class Health {
    private String name;
    private String description;
    private String phone;
    private String workTime;
    private String address;

    public Health(){
        this.name = "";
        this.description = "";
        this.phone = "";
        this.workTime = "";
        this.address = "";
    }

    public Health(String name, String description, String phone,
                      String workTime, String address){
        this.name = name;
        this.description = description;
        this.phone = phone;
        this.workTime = workTime;
        this.address = address;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String descriptionShort) {
        this.description = descriptionShort;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}