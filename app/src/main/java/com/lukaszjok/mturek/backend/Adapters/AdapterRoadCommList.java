package com.lukaszjok.mturek.backend.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.RoadCommQueue;

import java.util.ArrayList;


public class AdapterRoadCommList extends BaseAdapter {
    private static String DBG_TAG = "dbg";
    private Context ctx;
    private ArrayList<RoadCommQueue> data = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvDealName;
        public TextView tvNumOfTicketInQueue;
        public TextView tvCurrentTicket;
        public TextView tvLastTicket;
    }

    public AdapterRoadCommList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final RoadCommQueue item) {
        data.add(item);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public RoadCommQueue getItem(int position) {

        return data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        data = new ArrayList<RoadCommQueue>();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.road_comm_item, parent, false);


            holder = new ViewHolder();
            holder.tvDealName = convertView.findViewById(R.id.tv_deal_name_value);
            holder.tvNumOfTicketInQueue = convertView.findViewById(R.id.tv_num_of_tickets_in_queue_value);
            holder.tvCurrentTicket = convertView.findViewById(R.id.tv_current_ticket_value);
            holder.tvLastTicket = convertView.findViewById(R.id.tv_last_ticket_value);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvDealName.setText(data.get(position).getDealName());
        holder.tvNumOfTicketInQueue.setText(data.get(position).getNumOfTicketsInQueue());
        holder.tvCurrentTicket.setText(data.get(position).getCurrentTicket());
        holder.tvLastTicket.setText(data.get(position).getLastTicket());

        return convertView;
    }



}
