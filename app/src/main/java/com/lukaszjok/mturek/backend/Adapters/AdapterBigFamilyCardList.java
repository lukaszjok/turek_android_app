package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.BigFamilyCardItem;
import com.lukaszjok.mturek.backend.ItemObjects.Taxi;

import java.util.ArrayList;


/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterBigFamilyCardList extends BaseAdapter {
    private Context ctx;
    private ArrayList<BigFamilyCardItem> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvCompany;
        public TextView tvAddress;
        public TextView tvBenefits;

    }

    public AdapterBigFamilyCardList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final BigFamilyCardItem item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public BigFamilyCardItem getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<BigFamilyCardItem>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.big_family_card_item, parent, false);

            holder = new ViewHolder();
            holder.tvCompany = (TextView) convertView.findViewById(R.id.tv_big_family_card_company);
            holder.tvAddress = (TextView) convertView.findViewById(R.id.tv_big_family_card_address);
            holder.tvBenefits = (TextView) convertView.findViewById(R.id.tv_big_family_card_benefits);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvCompany.setText(mData.get(position).getCompany().toUpperCase());
        holder.tvAddress.setText(mData.get(position).getAddress());
        holder.tvBenefits.setText(mData.get(position).getBenefits());


        return convertView;
    }




}

