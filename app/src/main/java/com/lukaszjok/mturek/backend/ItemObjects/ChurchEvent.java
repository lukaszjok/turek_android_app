package com.lukaszjok.mturek.backend.ItemObjects;

public class ChurchEvent {
    private String name;
    private String description;

    public ChurchEvent(){
        this.name = "";
        this.description = "";
    }

    public ChurchEvent(String name, String description){
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
