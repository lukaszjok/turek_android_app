package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.Pharmacy;

import java.util.ArrayList;

/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterPharmaciesList extends BaseAdapter {
    private Context ctx;
    private ArrayList<Pharmacy> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvPharmaciesName;
        public TextView tvPharmaciesPhone;
        public TextView tvPharmaciesAddress;
        public TextView tvPharmaciesWorkTime;
    }

    public AdapterPharmaciesList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final Pharmacy item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Pharmacy getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<Pharmacy>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.pharmacy_item, parent, false);

            holder = new ViewHolder();
            holder.tvPharmaciesName = (TextView) convertView.findViewById(R.id.tv_pharmacy_name);
            holder.tvPharmaciesPhone = (TextView) convertView.findViewById(R.id.tv_pharmacy_phone);
            holder.tvPharmaciesAddress = (TextView) convertView.findViewById(R.id.tv_pharmacy_address);
            holder.tvPharmaciesWorkTime = (TextView) convertView.findViewById(R.id.tv_pharmacy_work_time);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvPharmaciesName.setText(mData.get(position).getName().toUpperCase());
        holder.tvPharmaciesPhone.setText(mData.get(position).getPhone());
        holder.tvPharmaciesAddress.setText(mData.get(position).getAddress());
        holder.tvPharmaciesWorkTime.setText(mData.get(position).getWorkTime().replaceAll("\\\\n", "\n"));


        return convertView;
    }




}

