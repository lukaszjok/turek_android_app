package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.AndroidLicense;

/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterAndroidLicensesList extends BaseAdapter {
    private Context ctx;
    private ArrayList<AndroidLicense> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvLibraryName;
        public TextView tvDescription;

    }

    public AdapterAndroidLicensesList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final AndroidLicense item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public AndroidLicense getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<AndroidLicense>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.android_license_item, parent, false);

            holder = new ViewHolder();
            holder.tvLibraryName = (TextView) convertView.findViewById(R.id.tv_android_license_library_name);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.tv_android_license_description);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvLibraryName.setText(mData.get(position).getLibraryName().toUpperCase());
        holder.tvDescription.setText(mData.get(position).getDescription().replaceAll("\\\\n", "\n"));



        return convertView;
    }




}

