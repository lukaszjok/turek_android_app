package com.lukaszjok.mturek.backend.ItemObjects;

public class Tur1921TurekScoreRow {
    private int rank;
    private String teamName;
    private int playedMatches;
    private int score;
    private String goals;

    public Tur1921TurekScoreRow(){
        this.rank = -1;
        this.teamName = "";
        this.playedMatches = -1;
        this.score = -1;
        this.goals = "";
    }

    public Tur1921TurekScoreRow(int rank, String teamName, int playedMatches, int score, String goals){
        this.rank = rank;
        this.teamName = teamName;
        this.playedMatches = playedMatches;
        this.score = score;
        this.goals = goals;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getPlayedMatches() {
        return playedMatches;
    }

    public void setPlayedMatches(int playedMatches) {
        this.playedMatches = playedMatches;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getGoals() {
        return goals;
    }

    public void setGoals(String goals) {
        this.goals = goals;
    }
}
