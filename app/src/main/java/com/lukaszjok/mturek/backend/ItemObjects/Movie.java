package com.lukaszjok.mturek.backend.ItemObjects;

public class Movie {
    private String title;
    private String moreInfo;
    private String date;
    private String time;

    public Movie(){
        this.title = "";
        this.moreInfo = "";
        this.date = "";
        this.time = "";
    }

    public Movie(String title, String moreInfo, String date, String time){
        this.title = title;
        this.moreInfo = moreInfo;
        this.date = date;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
