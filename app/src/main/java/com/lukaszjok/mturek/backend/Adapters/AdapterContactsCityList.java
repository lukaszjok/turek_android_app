package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.Contact;

import java.util.ArrayList;

/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterContactsCityList extends BaseAdapter {
    private Context ctx;
    private ArrayList<Contact> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvContactCityProfession;
        public TextView tvContactCityDepartment;
        public TextView tvContactCityAddress;
        public TextView tvContactCityNumber;
    }

    public AdapterContactsCityList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final Contact item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Contact getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<Contact>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.contact_city_item, parent, false);

            holder = new ViewHolder();
            holder.tvContactCityProfession = (TextView) convertView.findViewById(R.id.tv_contact_city_profession);
            holder.tvContactCityDepartment = (TextView) convertView.findViewById(R.id.tv_contact_city_department);
            holder.tvContactCityAddress = (TextView) convertView.findViewById(R.id.tv_contact_city_address);
            holder.tvContactCityNumber = (TextView) convertView.findViewById(R.id.tv_contact_city_number);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvContactCityProfession.setText(mData.get(position).getProfession().toUpperCase());
        holder.tvContactCityDepartment.setText(mData.get(position).getDepartment());
        holder.tvContactCityAddress.setText(mData.get(position).getAddress());
        holder.tvContactCityNumber.setText(mData.get(position).getNumber());


        return convertView;
    }




}

