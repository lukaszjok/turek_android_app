package com.lukaszjok.mturek.backend.ItemObjects;

public class Church {
    private int idChurch;
    private String name;
    private String type;
    private String phone;
    private String address;

    public Church(){
        this.idChurch = -1;
        this.name = "";
        this.type = "";
        this.phone = "";
        this.address = "";
    }

    public Church(int idChurch, String name, String type, String phone, String address){
        this.idChurch = idChurch;
        this.name = name;
        this.type = type;
        this.phone = phone;
        this.address = address;
    }

    public int getIdChurch() {
        return idChurch;
    }

    public void setIdChurch(int idChurch) {
        this.idChurch = idChurch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
