package com.lukaszjok.mturek.backend.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.GalleryPicture;

import java.util.ArrayList;

public class AdapterGalleryAttractionContentList extends BaseAdapter {
    private static String DBG_TAG = "dbg";
    private Context ctx;
    private ArrayList<GalleryPicture> data = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public ImageView imgGalleryPicture;
    }

    public AdapterGalleryAttractionContentList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final GalleryPicture item) {
        data.add(item);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public GalleryPicture getItem(int position) {

        return data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        data = new ArrayList<GalleryPicture>();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.gallery_attraction_content_item, parent, false);


            holder = new ViewHolder();
            holder.imgGalleryPicture = convertView.findViewById(R.id.img_gallery_attraction_content);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imgGalleryPicture.setImageBitmap(data.get(position).getBitmap());



        return convertView;
    }



}
