package com.lukaszjok.mturek.backend.ItemObjects;

public class AttractionContent {
    private String title;
    private String body;
    private String[] imagesUrls;

    public AttractionContent(){
        this.title="";
        this.body="";
        this.imagesUrls = new String[]{};
    }
    public AttractionContent(String title, String body, String imagesURLs){
        this.title=title;
        this.body=body;
        this.imagesUrls = imagesURLs.split(";;");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String[] getImagesUrls() {
        return imagesUrls;
    }

    public void setImagesUrls(String[] imagesUrls) {
        this.imagesUrls = imagesUrls;
    }
}
