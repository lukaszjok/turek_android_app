package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.Movie;

import java.util.ArrayList;



/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterCinemaMovieList extends BaseAdapter {
    private Context ctx;
    private ArrayList<Movie> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvCinemaMovieTitle;
        public TextView tvCinemaMovieMoreInfo;
        public TextView tvCinemaMovieDate;
        public TextView tvCinemaMovieTime;
    }

    public AdapterCinemaMovieList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final Movie item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Movie getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<Movie>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.cinema_movie_item, parent, false);

            holder = new ViewHolder();
            holder.tvCinemaMovieTitle = (TextView) convertView.findViewById(R.id.tv_cinema_item_title);
            holder.tvCinemaMovieMoreInfo = (TextView) convertView.findViewById(R.id.tv_cinema_item_more_info);
            holder.tvCinemaMovieDate = (TextView) convertView.findViewById(R.id.tv_cinema_item_date);
            holder.tvCinemaMovieTime = (TextView) convertView.findViewById(R.id.tv_cinema_item_time);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvCinemaMovieTitle.setText(mData.get(position).getTitle().toUpperCase());
        holder.tvCinemaMovieMoreInfo.setText(mData.get(position).getMoreInfo());
        holder.tvCinemaMovieDate.setText(mData.get(position).getDate());
        holder.tvCinemaMovieTime.setText(mData.get(position).getTime());

        return convertView;
    }




}

