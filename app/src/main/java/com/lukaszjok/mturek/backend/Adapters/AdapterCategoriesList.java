package com.lukaszjok.mturek.backend.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.Category;

import java.util.ArrayList;


public class AdapterCategoriesList extends BaseAdapter {
    private static String DBG_TAG = "dbg";
    private Context ctx;
    private ArrayList<Category> data = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvCategoryItem;
        public ImageView imgCategory;
    }

    public AdapterCategoriesList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final Category item) {
        data.add(item);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Category getItem(int position) {

        return data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        data = new ArrayList<Category>();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.category_item, parent, false);


            holder = new ViewHolder();
            holder.tvCategoryItem = convertView.findViewById(R.id.tv_catecory_name);
            holder.imgCategory = convertView.findViewById(R.id.img_category);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvCategoryItem.setText(data.get(position).getDisplayName());


        String idName = data.get(position).getIdName();

        if(idName.toLowerCase().equals("miasto_tel")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_contact_phone_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.correct));
        }else if(idName.toLowerCase().equals("karta_rodziny")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_child_care_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("imprezy")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_event_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("kino")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_local_movies_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("interwencja")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_warning_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("taxi")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_local_taxi_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("zdrowie")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_local_hospital_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("gastro")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_restaurant_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("oswiata")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_school_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("wydz_komunik")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_baseline_commute_24px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("apteki")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_local_pharmacy_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("tur_turek")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_fitness_center_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("prawnik")){
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_account_balance_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));

        }else if(idName.toLowerCase().equals("koscioly")) {
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_favorite_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("atrakcje")) {
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_place_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else if(idName.toLowerCase().equals("media")) {
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_public_48px));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.incorrect));
        }else{
            holder.imgCategory.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_menu_gallery));
            //convertView.setBackgroundColor(ctx.getResources().getColor(R.color.colorAccent));
        }



        return convertView;
    }



}
