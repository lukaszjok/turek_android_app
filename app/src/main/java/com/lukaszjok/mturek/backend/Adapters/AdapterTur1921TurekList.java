package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.Tur1921TurekScoreRow;

import java.util.ArrayList;


/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterTur1921TurekList extends BaseAdapter {
    private Context ctx;
    private ArrayList<Tur1921TurekScoreRow> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvTur1921TurekRank;
        public TextView tvTur1921TurekTeamName;
        public TextView tvTur1921TurekPlayedMatches;
        public TextView tvTur1921TurekScore;
        public TextView tvTur1921TurekGoals;
    }

    public AdapterTur1921TurekList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final Tur1921TurekScoreRow item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Tur1921TurekScoreRow getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<Tur1921TurekScoreRow>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.tur_1921_turek_item, parent, false);

            holder = new ViewHolder();
            holder.tvTur1921TurekRank = (TextView) convertView.findViewById(R.id.tv_tur_1921_turek_rank);
            holder.tvTur1921TurekTeamName = (TextView) convertView.findViewById(R.id.tv_tur_1921_turek_team_name);
            holder.tvTur1921TurekPlayedMatches = (TextView) convertView.findViewById(R.id.tv_tur_1921_turek_played_matches);
            holder.tvTur1921TurekScore = (TextView) convertView.findViewById(R.id.tv_tur_1921_turek_score);
            holder.tvTur1921TurekGoals = (TextView) convertView.findViewById(R.id.tv_tur_1921_turek_goals);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvTur1921TurekRank.setText(Integer.toString(mData.get(position).getRank()));
        holder.tvTur1921TurekTeamName.setText(mData.get(position).getTeamName());
        holder.tvTur1921TurekPlayedMatches.setText(Integer.toString(mData.get(position).getPlayedMatches()));
        holder.tvTur1921TurekScore.setText(Integer.toString(mData.get(position).getScore()));
        holder.tvTur1921TurekGoals.setText(mData.get(position).getGoals());


        return convertView;
    }




}

