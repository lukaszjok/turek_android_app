package com.lukaszjok.mturek.backend.ItemObjects;

public class Contact {
    private String profession;
    private String department;
    private String address;
    private String number;

    public Contact(){
        this.profession = "";
        this.department = "";
        this.number = "";
        this.address = "";
    }


    public Contact(String profession, String department, String address, String number){
        this.profession = profession;
        this.department = department;
        this.address = address;
        this.number = number;
    }



    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
