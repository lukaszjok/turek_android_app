package com.lukaszjok.mturek.backend.ItemObjects;

public class BigFamilyCardItem {
    private String company;
    private String address;
    private String benefits;

    public BigFamilyCardItem(){
        this.company = "";
        this.address = "";
        this.benefits = "";
    }

    public BigFamilyCardItem(String company, String address, String benefits){
        this.company = company;
        this.address = address;
        this.benefits = benefits;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBenefits() {
        return benefits;
    }

    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }
}
