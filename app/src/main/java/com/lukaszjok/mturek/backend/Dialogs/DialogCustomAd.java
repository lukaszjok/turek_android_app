package com.lukaszjok.mturek.backend.Dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import android.widget.ImageView;

public class DialogCustomAd extends DialogFragment {

    private String title;
    private String message;
    private byte[] imageBytes;
    private int imageWidth;
    private int imageHeight;


    public DialogCustomAdListener dialogCustomAdListener;
    private static final String TAG = "DialogDeleteAllQsos";

    public DialogCustomAd(){
    }

    public interface DialogCustomAdListener {
        void DialogCustomAdHandler(boolean input);
    }

    @Override

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (getArguments() != null) {
            title = getArguments().getString("title","");
            message = getArguments().getString("message","");
            imageBytes = getArguments().getByteArray("image");
            imageWidth = getArguments().getInt("imageWidth");
            imageHeight = getArguments().getInt("imageHeight");
        }

        dialogCustomAdListener = (DialogCustomAdListener) getActivity();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);

        ImageView imageView = new ImageView(getActivity().getApplicationContext());
        //Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.turek_mobi_logo);
        Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        imageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, imageWidth, imageHeight, false));
        alertDialogBuilder.setView(imageView);


        /*
        alertDialogBuilder.setPositiveButton("tak",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success
                dialogCustomAdListener.DialogCustomAdHandler(true);
            }
        });
        */
        alertDialogBuilder.setNegativeButton("ZAMKNIJ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogCustomAdListener.DialogCustomAdHandler(false);
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

        });





        return alertDialogBuilder.create();

    }

}
