package com.lukaszjok.mturek.backend.ItemObjects;

public class CityEvent {
    private String name;
    private String description;
    private String place;
    private String date;
    private String time;
    private String price;
    private String age_limit;
    private String url_link;
    
    public CityEvent(){
        this.name = "";
        this.description= "";
        this.place= "";
        this.date= "";
        this.time= "";
        this.price= "";
        this.age_limit= "";
        this.url_link= "";
    }

    public CityEvent(String name, String description, String place, String date, String time, String price, String age_limit,String url_link ){
        this.name = name;
        this.description= description;
        this.place= place;
        this.date= date;
        this.time= time;
        this.price= price;
        this.age_limit= age_limit;
        this.url_link= url_link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAge_limit() {
        return age_limit;
    }

    public void setAge_limit(String age_limit) {
        this.age_limit = age_limit;
    }

    public String getUrl_link() {
        return url_link;
    }

    public void setUrl_link(String url_link) {
        this.url_link = url_link;
    }
}
