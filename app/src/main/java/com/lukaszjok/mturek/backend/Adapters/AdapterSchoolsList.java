package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.School;

import java.util.ArrayList;


/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterSchoolsList extends BaseAdapter {
    private Context ctx;
    private ArrayList<School> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvSchoolName;
        public TextView tvSchoolTypeName;
        public TextView tvSchoolAddress;
        public TextView tvSchoolPhone;
        public TextView tvSchoolWebsite;

}

    public AdapterSchoolsList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final School item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public School getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<School>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.school_item, parent, false);

            holder = new ViewHolder();
            holder.tvSchoolName = (TextView) convertView.findViewById(R.id.tv_school_name);
            holder.tvSchoolTypeName = (TextView) convertView.findViewById(R.id.tv_school_type_name);
            holder.tvSchoolAddress = (TextView) convertView.findViewById(R.id.tv_school_address);
            holder.tvSchoolPhone = (TextView) convertView.findViewById(R.id.tv_school_phone);
            holder.tvSchoolWebsite = (TextView) convertView.findViewById(R.id.tv_school_website);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvSchoolName.setText(mData.get(position).getName().toUpperCase());
        holder.tvSchoolTypeName.setText(mData.get(position).getTypeName());
        holder.tvSchoolAddress.setText(mData.get(position).getAddress().replaceAll("\\\\n", "\n"));
        holder.tvSchoolPhone.setText(mData.get(position).getPhone());
        holder.tvSchoolWebsite.setText(mData.get(position).getWebsite());


        return convertView;
    }




}

