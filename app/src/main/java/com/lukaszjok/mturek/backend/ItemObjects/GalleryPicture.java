package com.lukaszjok.mturek.backend.ItemObjects;

import android.graphics.Bitmap;

public class GalleryPicture {
    private Bitmap bitmap;

    public GalleryPicture(){
        this.bitmap = null;
    }

    public GalleryPicture(Bitmap bitmap){
        this.bitmap = bitmap;
}

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
