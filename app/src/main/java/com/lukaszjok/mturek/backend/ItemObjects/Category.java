package com.lukaszjok.mturek.backend.ItemObjects;

public class Category {
    private String displayName;
    private String idName;

    public Category(){
        this.displayName = "";
        this.idName = "";
    }

    public Category(String displayName, String idName){
        this.displayName = displayName;
        this.idName = idName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }


}
