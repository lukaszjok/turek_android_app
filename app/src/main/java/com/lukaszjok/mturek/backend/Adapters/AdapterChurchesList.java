package com.lukaszjok.mturek.backend.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.ItemObjects.Church;

import java.util.ArrayList;


/**
 * Created by Łukasz on 24.02.2018.
 */

public class AdapterChurchesList extends BaseAdapter {
    private Context ctx;
    private ArrayList<Church> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvChurchName;
        public TextView tvChurchType;
        public TextView tvChurchPhone;
        public TextView tvChurchAddress;
    }

    public AdapterChurchesList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final Church item) {
        mData.add(item);
        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Church getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<Church>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.church_item, parent, false);

            holder = new ViewHolder();
            holder.tvChurchName = (TextView) convertView.findViewById(R.id.tv_church_name);
            holder.tvChurchType = (TextView) convertView.findViewById(R.id.tv_church_type);
            holder.tvChurchPhone = (TextView) convertView.findViewById(R.id.tv_church_phone);
            holder.tvChurchAddress = (TextView) convertView.findViewById(R.id.tv_church_address);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvChurchName.setText(mData.get(position).getName().toUpperCase());
        holder.tvChurchType.setText(mData.get(position).getType());
        holder.tvChurchPhone.setText(mData.get(position).getPhone());
        holder.tvChurchAddress.setText(mData.get(position).getAddress());


        return convertView;
    }




}

