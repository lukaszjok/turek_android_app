package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.Adapters.AdapterSchoolsList;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.backend.ItemObjects.School;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class SchoolsActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdView mAdView;
    private AdapterSchoolsList schoolsListAdapter;
    private ListView lvSchoolsList;
    private ArrayList<School> allSchools;
    private ProgressBar pbInitial;


    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schools);

        // Get ActionBar
        ActionBar actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_schools_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillChurchesItems(getApplicationContext());
        lvSchoolsList.setAdapter(schoolsListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        lvSchoolsList = findViewById(R.id.lv_schools);
        allSchools = new ArrayList<>();
        pbInitial = findViewById(R.id.progressBar_schools_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);


    }
    private void initListeners(){
        lvSchoolsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int schoolId = schoolsListAdapter.getItem(position).getId();

                String url = schoolsListAdapter.getItem(position).getWebsite().trim();
                if(url.length()>0){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                }

            }
        });

    }


    public void fillChurchesItems(Context ctx) {
        schoolsListAdapter = new AdapterSchoolsList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_schools", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("schools");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_schools: " + status);
                    Log.i(DBG_TAG,"code_schools: " + code);
                    Log.i(DBG_TAG,"desc_schools: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allSchools.add(new School(jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("type_name"),
                                jsonArray.getJSONObject(i).getString("address"),
                                jsonArray.getJSONObject(i).getString("phone"),
                                jsonArray.getJSONObject(i).getString("website")

                        ));
                    }

                    for (int i = 0; i < allSchools.size(); i++) {
                        schoolsListAdapter.addItem(allSchools.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvSchoolsList.setAdapter(schoolsListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, schools)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure schools JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillChurchesItems(SchoolsActivity.this);
                }else{
                    Toast.makeText(SchoolsActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the search menu action bar.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar_schools, menu);

        // Get the search menu.
        MenuItem searchMenu = menu.findItem(R.id.app_bar_menu_search_schools);

        // Get SearchView object.
        SearchView searchView = (SearchView) searchMenu.getActionView();

        // Below event is triggered when submit search query.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String inputWord) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String inputWord) {
                searcher(inputWord);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void searcher(String word){
        ArrayList<School> result = new ArrayList<>();
        schoolsListAdapter = new AdapterSchoolsList(this);

        for(School item: allSchools){
            if(item.getAddress().toLowerCase().contains(word.toLowerCase())||
                    item.getName().toLowerCase().contains(word.toLowerCase())||
                    item.getPhone().toLowerCase().contains(word.toLowerCase())||
                    item.getTypeName().toLowerCase().contains(word.toLowerCase())||
                    item.getTypeName().toLowerCase().contains(word.toLowerCase())){
                result.add(item);
            }
        }
        for (int i = 0; i < result.size(); i++) {
            schoolsListAdapter.addItem(result.get(i));
        }
        lvSchoolsList.setAdapter(schoolsListAdapter);
    }

}
