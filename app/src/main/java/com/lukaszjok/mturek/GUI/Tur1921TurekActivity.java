package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.Adapters.AdapterTur1921TurekList;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.backend.ItemObjects.Tur1921TurekScoreRow;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class Tur1921TurekActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdView mAdView;
    private AdapterTur1921TurekList tur1921TurekListAdapter;
    private ListView lvRur1921TurekList;
    private ArrayList<Tur1921TurekScoreRow> allTur1921Turek;
    private ProgressBar pbInitial;


    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tur1921_turek);

        // Get ActionBar
        ActionBar actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_tur1921turek_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------


        //--- Contacts City List (adapter) + loading data -----------------
        fillTur1921TurekItems(getApplicationContext());
        lvRur1921TurekList.setAdapter(tur1921TurekListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------

    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        lvRur1921TurekList = findViewById(R.id.lv_tur1921turek);
        allTur1921Turek = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_tur_turek_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);


    }
    private void initListeners(){

    }

    public void fillTur1921TurekItems(Context ctx) {
        tur1921TurekListAdapter = new AdapterTur1921TurekList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_tur_1921_turek_seniors", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("tur_1921_turek_seniors");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_tur1921turek: " + status);
                    Log.i(DBG_TAG,"code_tur1921turek: " + code);
                    Log.i(DBG_TAG,"desc_tur1921turek: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allTur1921Turek.add(new Tur1921TurekScoreRow(jsonArray.getJSONObject(i).getInt("ranking"),
                                jsonArray.getJSONObject(i).getString("team_name"),
                                jsonArray.getJSONObject(i).getInt("played_matches"),
                                jsonArray.getJSONObject(i).getInt("score"),
                                jsonArray.getJSONObject(i).getString("goals")
                        ));
                    }

                    for (int i = 0; i < allTur1921Turek.size(); i++) {
                        tur1921TurekListAdapter.addItem(allTur1921Turek.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvRur1921TurekList.setAdapter(tur1921TurekListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, tur1921turek)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure tur1921tureke JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillTur1921TurekItems(Tur1921TurekActivity.this);
                }else{
                    Toast.makeText(Tur1921TurekActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the search menu action bar.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar_tur_1921_turek, menu);

        // Get the search menu.
        MenuItem searchMenu = menu.findItem(R.id.app_bar_menu_search_tur_1921_turek);

        // Get SearchView object.
        SearchView searchView = (SearchView) searchMenu.getActionView();

        // Below event is triggered when submit search query.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String inputWord) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String inputWord) {
                searcher(inputWord);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void searcher(String word){
        ArrayList<Tur1921TurekScoreRow> result = new ArrayList<>();
        tur1921TurekListAdapter = new AdapterTur1921TurekList(this);

        for(Tur1921TurekScoreRow item: allTur1921Turek){
            if(item.getTeamName().toLowerCase().contains(word.toLowerCase())){
                result.add(item);
            }
        }
        for (int i = 0; i < result.size(); i++) {
            tur1921TurekListAdapter.addItem(result.get(i));
        }
        lvRur1921TurekList.setAdapter(tur1921TurekListAdapter);
    }
}
