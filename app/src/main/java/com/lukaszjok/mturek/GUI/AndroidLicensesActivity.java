package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.Adapters.AdapterAndroidLicensesList;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.backend.ItemObjects.AndroidLicense;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class AndroidLicensesActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdView mAdView;
    private AdapterAndroidLicensesList androidLicensesListAdapter;
    private ListView lvAndroidLicensesList;
    private ArrayList<AndroidLicense> allAndroidLicenses;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_licenses);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_android_licenses_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillAndroidLicenses(getApplicationContext());
        lvAndroidLicensesList.setAdapter(androidLicensesListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------

    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);

        lvAndroidLicensesList = findViewById(R.id.lv_android_licenses);
        allAndroidLicenses = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_android_licenses_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }
    private void initListeners(){

    }


    public void fillAndroidLicenses(Context ctx) {
        androidLicensesListAdapter = new AdapterAndroidLicensesList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_android_licenses", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("android_licenses");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_android_licenses: " + status);
                    Log.i(DBG_TAG,"code_android_licenses: " + code);
                    Log.i(DBG_TAG,"desc_android_licenses: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allAndroidLicenses.add(new AndroidLicense(jsonArray.getJSONObject(i).getString("library_name"),
                                jsonArray.getJSONObject(i).getString("description")
                        ));
                    }

                    for (int i = 0; i < allAndroidLicenses.size(); i++) {
                        androidLicensesListAdapter.addItem(allAndroidLicenses.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvAndroidLicensesList.setAdapter(androidLicensesListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, android_licenses");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure android_licenses city JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillAndroidLicenses(AndroidLicensesActivity.this);
                }else{
                    Toast.makeText(AndroidLicensesActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }


}