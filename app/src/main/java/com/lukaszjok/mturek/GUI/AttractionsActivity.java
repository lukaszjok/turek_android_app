package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.Adapters.AdapterAttractionsList;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.backend.ItemObjects.Attraction;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class AttractionsActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdView mAdView;
    private AdapterAttractionsList attractionsListAdapter;
    private ListView lvAttractionsList;
    private ArrayList<Attraction> allAttractions;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attraction);

        // Get ActionBar
        ActionBar actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_attractions_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillChurchesItems(getApplicationContext());
        lvAttractionsList.setAdapter(attractionsListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        lvAttractionsList = findViewById(R.id.lv_attractions);
        allAttractions = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_attraction_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }
    private void initListeners(){
        lvAttractionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int attractionId = attractionsListAdapter.getItem(position).getId();

                Intent intent = new Intent(AttractionsActivity.this, AttractionContentActivity.class);
                intent.putExtra("attraction_id", attractionId);
                Log.i(DBG_TAG,"In AttractionActivity, attraction_id = " + attractionId);
                startActivity(intent);
            }
        });

    }


    public void fillChurchesItems(Context ctx) {
        attractionsListAdapter = new AdapterAttractionsList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_attractions", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("attractions");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_attractions: " + status);
                    Log.i(DBG_TAG,"code_attractions: " + code);
                    Log.i(DBG_TAG,"desc_attractions: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allAttractions.add(new Attraction(jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("description_short")
                        ));
                    }

                    for (int i = 0; i < allAttractions.size(); i++) {
                        attractionsListAdapter.addItem(allAttractions.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvAttractionsList.setAdapter(attractionsListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, attractionss)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure attractions JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillChurchesItems(AttractionsActivity.this);
                }else{
                    Toast.makeText(AttractionsActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the search menu action bar.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar_attractions, menu);

        // Get the search menu.
        MenuItem searchMenu = menu.findItem(R.id.app_bar_menu_search_attractions);

        // Get SearchView object.
        SearchView searchView = (SearchView) searchMenu.getActionView();

        // Below event is triggered when submit search query.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String inputWord) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String inputWord) {
                searcher(inputWord);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void searcher(String word){
        ArrayList<Attraction> result = new ArrayList<>();
        attractionsListAdapter = new AdapterAttractionsList(this);

        for(Attraction item: allAttractions){
            if(item.getName().toLowerCase().contains(word.toLowerCase())||
                    item.getDescription_short().toLowerCase().contains(word.toLowerCase())){
                result.add(item);
            }
        }
        for (int i = 0; i < result.size(); i++) {
            attractionsListAdapter.addItem(result.get(i));
        }
        lvAttractionsList.setAdapter(attractionsListAdapter);
    }

}
