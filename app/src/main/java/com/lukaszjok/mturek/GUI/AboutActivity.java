package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class AboutActivity extends AppCompatActivity {

    private AdView mAdView;
    private AppContentPreferences appContentPreferences;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    private ArrayList<String> aboutContent;
    private ProgressBar pbInitial;
    private TextView tvAboutContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_about_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        fillAboutContentData(AboutActivity.this);
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        aboutContent = new ArrayList<>();
        tvAboutContent = findViewById(R.id.tv_about_content);
        pbInitial = findViewById(R.id.progressBar_about_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
    }

    private void initListeners(){

    }

    public void fillAboutContentData(Context ctx) {
        RequestParams params = new RequestParams();
        params.put("key","ABOUT_APP");

        RESTapi.get("get_dynamic_content", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("dynamic_content");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_about: " + status);
                    Log.i(DBG_TAG,"code_about: " + code);
                    Log.i(DBG_TAG,"desc_about: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        aboutContent.add(jsonArray.getJSONObject(i).getString("value"));
                    }
                    pbInitial.setVisibility(View.GONE);

                    tvAboutContent.setText(aboutContent.get(0).replace("\\n","\n").replace("\\t","\t"));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, AboutyActivity)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure AboutyActivity JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillAboutContentData(AboutActivity.this);
                }else{
                    Toast.makeText(AboutActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }
}
