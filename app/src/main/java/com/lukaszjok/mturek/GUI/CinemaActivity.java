package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.Adapters.AdapterCinemaMovieList;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.backend.ItemObjects.Movie;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class CinemaActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdView mAdView;
    private AdapterCinemaMovieList cinemaMoviesAdapter;
    private ListView lvCinemaMovissList;
    private ArrayList<Movie> allMovies;
    private ProgressBar pbInitial;


    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinema);

        // Get ActionBar
        ActionBar actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);

        initControlsAndVariables();
        initListeners();
        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_cinema_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillCinemaMoviesItems(getApplicationContext());
        lvCinemaMovissList.setAdapter(cinemaMoviesAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        lvCinemaMovissList = findViewById(R.id.lv_cinema);
        allMovies = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_cinema_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }

    private void initListeners(){

    }

    public void fillCinemaMoviesItems(Context ctx) {
        cinemaMoviesAdapter = new AdapterCinemaMovieList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_cinema_movies", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("movies");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_cinema_movies: " + status);
                    Log.i(DBG_TAG,"code_cinema_movies: " + code);
                    Log.i(DBG_TAG,"desc_cinema_movies: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allMovies.add(new Movie(
                                jsonArray.getJSONObject(i).getString("title"),
                                jsonArray.getJSONObject(i).getString("more_info"),
                                jsonArray.getJSONObject(i).getString("date"),
                                jsonArray.getJSONObject(i).getString("time")
                        ));
                    }

                    for (int i = 0; i < allMovies.size(); i++) {
                        cinemaMoviesAdapter.addItem(allMovies.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvCinemaMovissList.setAdapter(cinemaMoviesAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, get_cinema_movies)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure get_cinema_movies JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillCinemaMoviesItems(CinemaActivity.this);
                }else{
                    Toast.makeText(CinemaActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the search menu action bar.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar_cinema, menu);

        // Get the search menu.
        MenuItem searchMenu = menu.findItem(R.id.app_bar_menu_search_cinema);

        // Get SearchView object.
        SearchView searchView = (SearchView) searchMenu.getActionView();

        // Below event is triggered when submit search query.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String inputWord) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String inputWord) {
                searcher(inputWord);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void searcher(String word){
        ArrayList<Movie> result = new ArrayList<>();
        cinemaMoviesAdapter = new AdapterCinemaMovieList(this);

        for(Movie item: allMovies){
            if(item.getDate().toLowerCase().contains(word.toLowerCase())||
                    item.getMoreInfo().toLowerCase().contains(word.toLowerCase())||
                    item.getTime().toLowerCase().contains(word.toLowerCase())||
                    item.getTitle().toLowerCase().contains(word.toLowerCase())){
                result.add(item);
            }
        }
        for (int i = 0; i < result.size(); i++) {
            cinemaMoviesAdapter.addItem(result.get(i));
        }
        lvCinemaMovissList.setAdapter(cinemaMoviesAdapter);
    }

}
