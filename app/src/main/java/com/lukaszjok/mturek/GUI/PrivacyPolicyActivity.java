package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class PrivacyPolicyActivity extends AppCompatActivity {

    private AdView mAdView;
    private AppContentPreferences appContentPreferences;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    private ArrayList<String> privacyPolicyContent;
    private ProgressBar pbInitial;
    private TextView tvPrivacyPolicyContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_privacy_policy_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        fillPrivacyPolicyContentData(PrivacyPolicyActivity.this);
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        privacyPolicyContent = new ArrayList<>();
        tvPrivacyPolicyContent = findViewById(R.id.tv_privacy_policy_content);

        pbInitial = findViewById(R.id.progressBar_privacy_policy_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }

    private void initListeners(){

    }

    public void fillPrivacyPolicyContentData(Context ctx) {
        RequestParams params = new RequestParams();
        params.put("key","PRIVACY_POLICY_CONTENT");

        RESTapi.get("get_dynamic_content", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("dynamic_content");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_privacy_policy: " + status);
                    Log.i(DBG_TAG,"code_privacy_policy: " + code);
                    Log.i(DBG_TAG,"desc_privacy_policy: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        privacyPolicyContent.add(jsonArray.getJSONObject(i).getString("value"));
                    }
                    pbInitial.setVisibility(View.GONE);

                    tvPrivacyPolicyContent.setText(privacyPolicyContent.get(0).replace("\\n","\n").replace("\\t","\t"));


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, PrivacyPolicyActivity)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure PrivacyPolicyActivity JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillPrivacyPolicyContentData(PrivacyPolicyActivity.this);
                }else{
                    Toast.makeText(PrivacyPolicyActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }


}
