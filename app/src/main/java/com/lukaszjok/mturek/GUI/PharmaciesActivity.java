package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.Adapters.AdapterPharmaciesList;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.backend.ItemObjects.Pharmacy;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class PharmaciesActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdView mAdView;
    private AdapterPharmaciesList pharmaciesListAdapter;
    private ListView lvPharmaciesList;
    private ArrayList<Pharmacy> allPharmacies;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacies);

        // Get ActionBar
        ActionBar actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_pharmacies_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------


        //--- Contacts City List (adapter) + loading data -----------------
        fillPharamciesItems(getApplicationContext());
        lvPharmaciesList.setAdapter(pharmaciesListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        lvPharmaciesList = findViewById(R.id.lv_pharmacies);
        allPharmacies = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_pharmacies_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }
    private void initListeners(){
        lvPharmaciesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String number = pharmaciesListAdapter.getItem(position).getPhone();

                number = number.trim();
                if(number.contains(",")){
                    number = number.substring(0,number.indexOf(","));
                }

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+number));
                startActivity(intent);
            }
        });

    }

    public void fillPharamciesItems(Context ctx) {
        pharmaciesListAdapter = new AdapterPharmaciesList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_pharmacies", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("pharmacies");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_pharmacies: " + status);
                    Log.i(DBG_TAG,"code_pharmacies: " + code);
                    Log.i(DBG_TAG,"desc_pharmacies: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allPharmacies.add(new Pharmacy(jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("phone"),
                                jsonArray.getJSONObject(i).getString("work_time"),
                                jsonArray.getJSONObject(i).getString("address")
                        ));
                    }

                    for (int i = 0; i < allPharmacies.size(); i++) {
                        pharmaciesListAdapter.addItem(allPharmacies.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvPharmaciesList.setAdapter(pharmaciesListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, pharmacies)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure pharmacies JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillPharamciesItems(PharmaciesActivity.this);
                }else{
                    Toast.makeText(PharmaciesActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the search menu action bar.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar_pharmacies, menu);

        // Get the search menu.
        MenuItem searchMenu = menu.findItem(R.id.app_bar_menu_search_pharmacies);

        // Get SearchView object.
        SearchView searchView = (SearchView) searchMenu.getActionView();

        // Below event is triggered when submit search query.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String inputWord) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String inputWord) {
                searcher(inputWord);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void searcher(String word){
        ArrayList<Pharmacy> result = new ArrayList<>();
        pharmaciesListAdapter = new AdapterPharmaciesList(this);

        for(Pharmacy item: allPharmacies){
            if(item.getAddress().toLowerCase().contains(word.toLowerCase())||
                    item.getName().toLowerCase().contains(word.toLowerCase())||
                    item.getPhone().toLowerCase().contains(word.toLowerCase())||
                    item.getWorkTime().toLowerCase().contains(word.toLowerCase())){
                result.add(item);
            }
        }
        for (int i = 0; i < result.size(); i++) {
            pharmaciesListAdapter.addItem(result.get(i));
        }
        lvPharmaciesList.setAdapter(pharmaciesListAdapter);
    }

}