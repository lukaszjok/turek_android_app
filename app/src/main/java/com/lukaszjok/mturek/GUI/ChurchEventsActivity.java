package com.lukaszjok.mturek.GUI;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.mturek.R;
import com.lukaszjok.mturek.backend.Adapters.AdapterChurchEventsList;
import com.lukaszjok.mturek.backend.AppContentPreferences;
import com.lukaszjok.mturek.backend.ItemObjects.ChurchEvent;
import com.lukaszjok.mturek.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class ChurchEventsActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdView mAdView;
    private AdapterChurchEventsList churchEventsListAdapter;
    private ListView lvChurchEventsList;
    private ArrayList<ChurchEvent> allChurchEvents;
    private int churchId;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_church_events);
        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        mAdView = findViewById(R.id.adView_church_events_activity);
        AdRequest adRequest;
        if(appContentPreferences.getAreTestAds().equals("1")){
            adRequest = new AdRequest.Builder().addTestDevice("FF112531D4B8AC442E90E0E37EBA82FC").build();
        }else{
            adRequest = new AdRequest.Builder().build();
        }
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------


        //--- Contacts City List (adapter) + loading data -----------------
        fillChurchEventsItems(getApplicationContext());
        lvChurchEventsList.setAdapter(churchEventsListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------

    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        lvChurchEventsList = findViewById(R.id.lv_church_events);
        allChurchEvents = new ArrayList<>();
        churchId=getIntent().getIntExtra("church_id",-100);
        Log.i(DBG_TAG,"In ChurchEventsActivity, church_id = " + churchId);

        pbInitial = findViewById(R.id.progressBar_church_events_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);


    }
    private void initListeners(){

    }

    public void fillChurchEventsItems(Context ctx) {
        churchEventsListAdapter = new AdapterChurchEventsList(this);
        RequestParams params = new RequestParams();
        Log.i(DBG_TAG,"In ChurchEventsActivity,as string church_id = " + Integer.toString(churchId));
        params.put("church_id", Integer.toString(churchId));

        RESTapi.get("get_church_events", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("church_events");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_church_events: " + status);
                    Log.i(DBG_TAG,"code_church_events: " + code);
                    Log.i(DBG_TAG,"desc_church_events: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allChurchEvents.add(new ChurchEvent(jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("description")
                        ));
                    }

                    for (int i = 0; i < allChurchEvents.size(); i++) {
                        churchEventsListAdapter.addItem(allChurchEvents.get(i));
                        Log.i(DBG_TAG,"--> " + allChurchEvents.get(i).getName());
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvChurchEventsList.setAdapter(churchEventsListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, church_events)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure church_events JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillChurchEventsItems(ChurchEventsActivity.this);
                }else{
                    Toast.makeText(ChurchEventsActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }
}
