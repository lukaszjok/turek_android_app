package com.lukaszjok.mturek.webservice;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

public class RESTapi {


    private static AsyncHttpClient myClient;
    private static PersistentCookieStore myCookieStore;
    private static String BASE_URL;

    //private static AsyncHttpClient client = new AsyncHttpClient();

    public static void initAPI(Context ctx, String url){
        try{
            BASE_URL = url;
            myClient = new AsyncHttpClient(true, 80, 443);
            myClient.setTimeout(5 * 1000); //in seconds
            myCookieStore = new PersistentCookieStore(ctx);
        }catch (Exception e){
            Log.i("dbg","problem z initem ");
        }
    }


    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {

        myClient.get(getAbsoluteUrl(url), params, responseHandler);
        myClient.setCookieStore(myCookieStore);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        myClient.post(getAbsoluteUrl(url), params, responseHandler);
        myClient.setCookieStore(myCookieStore);
    }

    public static void delete(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        myClient.delete(getAbsoluteUrl(url), params, responseHandler);
        myClient.setCookieStore(myCookieStore);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }


    public static void get(String url, BinaryHttpResponseHandler binaryHttpResponseHandler) {
        myClient.get(url, binaryHttpResponseHandler);
        myClient.setCookieStore(myCookieStore);
    }
}
