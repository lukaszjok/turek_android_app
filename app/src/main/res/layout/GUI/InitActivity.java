package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.BuildConfig;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.AppContentPreferences;
import lukaszjok.com.mturek.webservice.RESTapi;


public class InitActivity extends AppCompatActivity {
    private static String DBG_TAG = "dbg";
    private int failedHttpRequestCounter;
    private AppContentPreferences appContentPreferences;
    private static int failedHttpRequestConuter = 0;
    private ProgressBar pbInitial;
    private TextView tvInitMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        initControlsAndVariables();

        getProperiesFromRemoteConfig();



    }

    private void getProperiesFromRemoteConfig(){
        AsyncHttpClient httpClient = new AsyncHttpClient();
        httpClient.get("http://joksch.pl/turek-app/config_android.json", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("config");
                    String web_service_ip =  jResult.getString("web_service_ip");

                    Log.i(DBG_TAG,"status_web_service_ip: " + web_service_ip);

                    RESTapi.initAPI(lukaszjok.com.mturek.GUI.InitActivity.this, web_service_ip);
                    failedHttpRequestCounter = 0;
                    isAppAvailable(lukaszjok.com.mturek.GUI.InitActivity.this);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error InitActivity, config_err #1");
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure InitActivity, config_err #2" + throwable.getMessage());
                failedHttpRequestCounter++;

                if(failedHttpRequestCounter <3){
                    getProperiesFromRemoteConfig();
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.InitActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestCounter=0;
                }

            }

        });
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        pbInitial = findViewById(R.id.progressBar_init_activity);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
        tvInitMessage = findViewById(R.id.tv_init_message);

    }

    private void isAppAvailable(final Context ctx) {

        RequestParams params = new RequestParams();

        RESTapi.get("app_availability", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_is_AppAvailable: " + status);
                    Log.i(DBG_TAG,"code_is_AppAvailable: " + code);
                    Log.i(DBG_TAG,"desc_is_AppAvailable: " + desc);

                    if(code.equals("1") && status.equals("ok")){
                        getAndroidAppVersion();
                    }else if(code.equals("-1") && status.equals("not_ok")){
                        Toast.makeText(ctx, "Przerwa techniczna. Aplikacja niedostępna.",Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(ctx, "Problem z połączeniem",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error InitActivity, isUserLoggedInWebService() #1");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure InitActivity, isUserLoggedInWebService() #2" + throwable.getMessage());
                failedHttpRequestCounter++;

                if(failedHttpRequestCounter <3){
                    isAppAvailable(ctx);
                }else{
                    Toast.makeText(ctx, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestCounter=0;
                }
            }

        });

    }

    private void getAppContentParameters(){
        RequestParams params = new RequestParams();
        RESTapi.get("get_initial_content_parameters", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                Map<String, String> contentParametersDict = new HashMap<String, String>();
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("initial_content_parameters");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_content_parameters: " + status);
                    Log.i(DBG_TAG,"code_content_parameters: " + code);
                    Log.i(DBG_TAG,"desc_content_parameters: " + desc);


                    for(int i=0; i<jsonArray.length();i++){

                        contentParametersDict.put(jsonArray.getJSONObject(i).getString("ckey"),
                                jsonArray.getJSONObject(i).getString("cvalue"));
                    }

                    appContentPreferences.setInterventionDescription(contentParametersDict.get("INTERVENTION_DESCRIPTION"));
                    appContentPreferences.setInterventionCallNumber(contentParametersDict.get("INTERVENTION_CALL_NUMBER"));
                    appContentPreferences.setInterventionEmail(contentParametersDict.get("INTERVENTION_EMAIL"));

                    appContentPreferences.setIsMainAlertVissable(contentParametersDict.get("IS_MAIN_ALERT_VISSABLE"));
                    appContentPreferences.setMainAlert(contentParametersDict.get("MAIN_ALERT"));

                    appContentPreferences.setAppContactMail(contentParametersDict.get("APP_CONTACT_MAIL"));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, content_parameters()");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure content_parameters JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    getAppContentParameters();
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.InitActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });


    }


    private void getAndroidAppVersion(){
        RequestParams params = new RequestParams();
        RESTapi.get("get_android_app_version", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                Map<String, String> contentParametersDict = new HashMap<String, String>();
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("android_app_version");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_android_app_version: " + status);
                    Log.i(DBG_TAG,"code_android_app_version: " + code);
                    Log.i(DBG_TAG,"desc_android_app_versions: " + desc);

                    String version = jsonArray.getJSONObject(0).getString("android_app_version");

                    if(version.equals(BuildConfig.VERSION_NAME)){
                        getAppContentParameters();

                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.InitActivity.this, MainActivity.class);
                        startActivity(intent);
                        lukaszjok.com.mturek.GUI.InitActivity.this.finish();

                    }else{//do if user has got old version of application
                        pbInitial.setVisibility(View.INVISIBLE);
                        tvInitMessage.setText("Stara wersja aplikacji.\nPobierz najnowszą wersję i zaktualizuj oprogramowanie!");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, android_app_version");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure android_app_version JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    getAndroidAppVersion();
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.InitActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });


    }


}


