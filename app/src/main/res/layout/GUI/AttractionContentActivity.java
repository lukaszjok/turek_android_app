package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.Adapters.AdapterGalleryAttractionContentList;
import lukaszjok.com.mturek.backend.ItemObjects.AttractionContent;
import lukaszjok.com.mturek.backend.ItemObjects.GalleryPicture;
import lukaszjok.com.mturek.webservice.RESTapi;

public class AttractionContentActivity extends AppCompatActivity {
    private int attractionId;
    private static String DBG_TAG = "dbg";
    private ArrayList<AttractionContent> attractionContentData;

    private TextView tvAttractionContentTitle;
    private TextView tvAttractionContentBody;
    private ProgressBar pbInitial;

    private AdView mAdView;
    private static int failedHttpRequestConuter = 0;

    //-- gallery below:
    private AdapterGalleryAttractionContentList galleryListAdapter;
    private GridView lvGalleryList;
    private ArrayList<GalleryPicture> allPicture;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attraction_content);
        initControlsAndVariables();
        initListeners();

        fillAttracionContentData(lukaszjok.com.mturek.GUI.AttractionContentActivity.this);

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_attraction_content_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------



    }

    private void initControlsAndVariables(){
        tvAttractionContentTitle = findViewById(R.id.tv_attraction_content_header);
        tvAttractionContentBody = findViewById(R.id.attraction_content_body);

        attractionContentData = new ArrayList<>();

        attractionId=getIntent().getIntExtra("attraction_id",-100);
        Log.i(DBG_TAG,"In AttarctionContentActivity, attraction_id = " + attractionId);

        // for gallery:
        lvGalleryList = findViewById(R.id.lv_gallery_attraction_content);
        allPicture = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_attraction_content_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);




    }
    private void initListeners(){

    }

    public void fillAttracionContentData(Context ctx) {
        RequestParams params = new RequestParams();
        params.put("attraction_id", Integer.toString(attractionId));


        RESTapi.get("get_attraction_content", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("attraction_content");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_attraction_content: " + status);
                    Log.i(DBG_TAG,"code_attraction_content: " + code);
                    Log.i(DBG_TAG,"desc_attraction_content: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        attractionContentData.add(new AttractionContent(jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("description"),
                                jsonArray.getJSONObject(i).getString("images_links")
                        ));
                    }
                    pbInitial.setVisibility(View.GONE);

                    tvAttractionContentTitle.setText(attractionContentData.get(0).getTitle().replaceAll("\\\\n", "\n"));
                    tvAttractionContentBody.setText(attractionContentData.get(0).getBody().replaceAll("\\\\n", "\n"));

                    //--- Gallery List (adapter) + loading data -----------------
                    fillGalleryItems(getApplicationContext());
                    lvGalleryList.setAdapter(galleryListAdapter);
                    initListeners();
                    //--- END Gallery (adapter) + loading data -----------------


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, attraction_content)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure attraction_content JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillAttracionContentData(lukaszjok.com.mturek.GUI.AttractionContentActivity.this);
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.AttractionContentActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }

    public void fillGalleryItems(Context ctx) {
        galleryListAdapter = new AdapterGalleryAttractionContentList(this);
        RequestParams params = new RequestParams();

        String[] urls= attractionContentData.get(0).getImagesUrls();

        /*
        ArrayList<String> urls= new ArrayList<>();

        urls.add("https://o.turek.net.pl/!pliki/!ogl/20190217/1042_0_studnie-glebinowe-odwierty-kompleksowo-z-.jpg");
        urls.add("https://m.turek.net.pl/!pliki/20190402/15542173430_niebieski-happening-czyli-nawiatowy-.jpg");
        urls.add("https://o.turek.net.pl/!pliki/!ogl/20190217/1042_1_studnie-glebinowe-odwierty-kompleksowo-z-.jpg");
        */


        for(int i=0;i<urls.length;i++){
            RESTapi.get(urls[i], new BinaryHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(binaryData, 0, binaryData.length);

                    allPicture.add(new GalleryPicture(bitmap));
                    //get last image
                    galleryListAdapter.addItem(allPicture.get(allPicture.size()-1));

                    lvGalleryList.setAdapter(galleryListAdapter);



                }
                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {
                    Log.e(DBG_TAG,"ERROR, "+error.getMessage());

                }
            });
        }








    }



}
