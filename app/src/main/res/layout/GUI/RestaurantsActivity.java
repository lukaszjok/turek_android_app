package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.Adapters.AdapterRestaurantList;
import lukaszjok.com.mturek.backend.ItemObjects.Restaurant;
import lukaszjok.com.mturek.webservice.RESTapi;

public class RestaurantsActivity extends AppCompatActivity {
    private AdView mAdView;
    private AdapterRestaurantList restaurantsListAdapter;
    private ListView lvRestaurantsList;
    private ArrayList<Restaurant> allRestaurants;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurants);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_restaurants_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillRestaurantsItems(getApplicationContext());
        lvRestaurantsList.setAdapter(restaurantsListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------
    }

    private void initControlsAndVariables(){
        lvRestaurantsList = findViewById(R.id.lv_restaurants);
        allRestaurants = new ArrayList<>();
        pbInitial = findViewById(R.id.progressBar_restaurants_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }
    private void initListeners(){

        lvRestaurantsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int idRestaurant = restaurantsListAdapter.getItem(position).getId();
                String number = restaurantsListAdapter.getItem(position).getPhone();

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+number));
                startActivity(intent);
            }
        });


    }


    public void fillRestaurantsItems(Context ctx) {
        restaurantsListAdapter = new AdapterRestaurantList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_restaurants", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("restaurants");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_restaurants: " + status);
                    Log.i(DBG_TAG,"code_restaurants: " + code);
                    Log.i(DBG_TAG,"desc_restaurants: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allRestaurants.add(new Restaurant(jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("description_short"),
                                jsonArray.getJSONObject(i).getString("phone"),
                                jsonArray.getJSONObject(i).getString("min_order_cost"),
                                jsonArray.getJSONObject(i).getString("work_time"),
                                jsonArray.getJSONObject(i).getString("address")
                        ));
                    }

                    for (int i = 0; i < allRestaurants.size(); i++) {
                        restaurantsListAdapter.addItem(allRestaurants.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvRestaurantsList.setAdapter(restaurantsListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, restaurants)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure restaurants JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillRestaurantsItems(lukaszjok.com.mturek.GUI.RestaurantsActivity.this);
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.RestaurantsActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }

}
