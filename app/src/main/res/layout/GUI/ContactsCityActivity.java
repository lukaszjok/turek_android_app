package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.Adapters.AdapterContactsCityList;
import lukaszjok.com.mturek.backend.ItemObjects.Contact;
import lukaszjok.com.mturek.webservice.RESTapi;

public class ContactsCityActivity extends AppCompatActivity {
    private AdView mAdView;
    private AdapterContactsCityList contactsCityListAdapter;
    private ListView lvContactsCityList;
    private ArrayList<Contact> allContacts;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_city);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_contacts_city_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillContactsCityItems(getApplicationContext());
        lvContactsCityList.setAdapter(contactsCityListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------

    }

    private void initControlsAndVariables(){
        lvContactsCityList = findViewById(R.id.lv_contacts_city);
        allContacts = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_contacts_city_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }
    private void initListeners(){
        lvContactsCityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String number = contactsCityListAdapter.getItem(position).getNumber();

                number = number.trim();
                if(number.contains(",")){
                    number = number.substring(0,number.indexOf(","));
                }

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+number));
                startActivity(intent);
            }
        });

    }


    public void fillContactsCityItems(Context ctx) {
        contactsCityListAdapter = new AdapterContactsCityList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_contacts_city", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("contacts_city");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_contacts_city: " + status);
                    Log.i(DBG_TAG,"code_contacts_city: " + code);
                    Log.i(DBG_TAG,"desc_contacts_city: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allContacts.add(new Contact(jsonArray.getJSONObject(i).getString("profession"),
                                jsonArray.getJSONObject(i).getString("department"),
                                jsonArray.getJSONObject(i).getString("address"),
                                jsonArray.getJSONObject(i).getString("number")
                        ));
                    }

                    for (int i = 0; i < allContacts.size(); i++) {
                        contactsCityListAdapter.addItem(allContacts.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvContactsCityList.setAdapter(contactsCityListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, contacts city)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure contacts city JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillContactsCityItems(lukaszjok.com.mturek.GUI.ContactsCityActivity.this);
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.ContactsCityActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }


}
