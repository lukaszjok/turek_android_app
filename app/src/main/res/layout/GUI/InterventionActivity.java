package lukaszjok.com.mturek.GUI;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.AppContentPreferences;

public class InterventionActivity extends AppCompatActivity {

    private AdView mAdView;
    private AppContentPreferences appContentPreferences;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";
    private Button btnInterventionSendEmail;
    private Button btnInterventionCall;
    private TextView tvInterventionDescribe;
    private ProgressBar pbInitial;

    private String interventionCallNumber;
    private String interventionEmail;
    private String getInterventionDescribe;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intervention);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_intervention_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);

        pbInitial = findViewById(R.id.progressBar_intervention_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        btnInterventionCall = findViewById(R.id.btn_intervention_call);
        btnInterventionSendEmail = findViewById(R.id.btn_intervention_email);
        tvInterventionDescribe = findViewById(R.id.tv_intervention_describe);

        interventionCallNumber = appContentPreferences.getInterventionCallNumber();
        interventionEmail=appContentPreferences.getInterventionEmail();
        getInterventionDescribe=appContentPreferences.getInterventionDescription();

        tvInterventionDescribe.setText(getInterventionDescribe);
        pbInitial.setVisibility(View.GONE);



    }

    private void initListeners() {
        btnInterventionCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+interventionCallNumber));
                startActivity(intent);
                //InterventionActivity.this.finish();

            }
        });

        btnInterventionSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();

            }
        });

    }

    protected void sendEmail() {
        Log.i("Send email", "");

        String[] TO = {interventionEmail};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Interwencja: ");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Redaguj wiadomość... ");

        try {
            startActivity(Intent.createChooser(emailIntent, "Wyślij e-mail..."));
            Log.i("dbg", "Finished sending email...");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(lukaszjok.com.mturek.GUI.InterventionActivity.this,
                    R.string.toast_to_email_client_installed, Toast.LENGTH_SHORT).show();
        }
    }


}
