package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.Adapters.AdapterCategoriesList;
import lukaszjok.com.mturek.backend.AppContentPreferences;
import lukaszjok.com.mturek.backend.ItemObjects.Category;
import lukaszjok.com.mturek.webservice.RESTapi;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AppContentPreferences appContentPreferences;
    private AdView mAdView;
    private TextView tvMainAlert;
    private AdapterCategoriesList categoriesListAdapter;
    private GridView gvCategoriesList;
    private static int failedHttpRequestConuter = 0;
    private boolean doubleBackToExitPressedOnce = false;
    private ArrayList<Category> allCategories;
    private ProgressBar pbInitial;

    private static String DBG_TAG = "dbg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initControlsAndVariables();
        initListeners();


        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_main_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //--- Incidents List (adapter) + loading data -----------------
        fillCategoriesItems(getApplicationContext());
        gvCategoriesList.setAdapter(categoriesListAdapter);
        //--- END Incidents List (adapter) + loading data -----------------

    }

    private void initListeners() {
        gvCategoriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position,
                                    long arg3) {
                String choice = categoriesListAdapter.getItem(position).getIdName().toLowerCase();

                switch (choice){
                    case "interwencja": {
                        Log.i(DBG_TAG,"go to: interwencja");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.InterventionActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "miasto_tel": {
                        Log.i(DBG_TAG,"go to telefony");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.ContactsCityActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "wydz_komunik": {
                        Log.i(DBG_TAG,"go to: wydzial komunikacji");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.RoadCommunicationDepActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "kino": {
                        Log.i(DBG_TAG,"go to: kino");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.CinemaActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "taxi": {
                        Log.i(DBG_TAG,"go to: taxi");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.TaxiActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "gastro": {
                        Log.i(DBG_TAG,"go to: gastro");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.RestaurantsActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "koscioly": {
                        Log.i(DBG_TAG,"go to: koscioly");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.ChurchesActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "zdrowie": {
                        Log.i(DBG_TAG,"go to: zdrowie");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.HealthActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "apteki": {
                        Log.i(DBG_TAG,"go to: apteki");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, PharmaciesActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "tur_turek": {
                        Log.i(DBG_TAG,"go to: tur_turek");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.Tur1921TurekActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "oswiata": {
                        Log.i(DBG_TAG,"go to: oswiata");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.SchoolsActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "atrakcje": {
                        Log.i(DBG_TAG,"go to: atrakcje");
                        Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.AttractionsActivity.class);
                        startActivity(intent);
                        break;
                    }

                    default: {
                        Log.i(DBG_TAG,"default");
                        break;
                    }
                }
            }
        });
    }

    private void initControlsAndVariables() {
        appContentPreferences = new AppContentPreferences(this);
        tvMainAlert = findViewById(R.id.tv_main_alert);

        if(appContentPreferences.getIsMainAlertVissable().equals("1")){
            tvMainAlert.setVisibility(View.VISIBLE);
            tvMainAlert.setText(appContentPreferences.getMainAlert());
        }else {
            tvMainAlert.setVisibility(View.GONE);
            tvMainAlert.setText("");
        }

        gvCategoriesList = findViewById(R.id.gv_categories);
        allCategories = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_main_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }

    public void fillCategoriesItems(Context ctx) {
        categoriesListAdapter = new AdapterCategoriesList(this);
        RequestParams params = new RequestParams();


        RESTapi.get("get_app_categories", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("app_categories");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_get_app_categories: " + status);
                    Log.i(DBG_TAG,"code_get_app_categories: " + code);
                    Log.i(DBG_TAG,"desc_get_app_categories: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allCategories.add(new Category(jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("id_name")
                        ));
                    }

                    for (int i = 0; i < allCategories.size(); i++) {
                        categoriesListAdapter.addItem(allCategories.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    gvCategoriesList.setAdapter(categoriesListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, getIncidentsNearest()");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure get nearest incident JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillCategoriesItems(lukaszjok.com.mturek.GUI.MainActivity.this);
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.MainActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

        for(int i=0; i<allCategories.size();i++){
            Log.i(DBG_TAG,allCategories.get(i).toString());
        }

    }

    @Override
    public void onBackPressed() {

        //--- START: Double click (till 2 seconds) to exit application
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Kliknij ponownie WSTECZ, żeby zamknąć.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
        //--- END: Double click (till 2 seconds) to exit application.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }+
        */

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_privacy_policy) {
            Log.i(DBG_TAG,"go to: privacy_policy");
            Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.PrivacyPolicyActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_about) {
            Log.i(DBG_TAG,"go to: about");
            Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.AboutActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_android_licenses) {
            Log.i(DBG_TAG,"go to: android_licenses");
            Intent intent = new Intent(lukaszjok.com.mturek.GUI.MainActivity.this, lukaszjok.com.mturek.GUI.AndroidLicensesActivity.class);
            startActivity(intent);

        }else if (id == R.id.nav_contact) {
            sendEmail();

        } else if (id == R.id.nav_vote_app) {

            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected void sendEmail() {
        Log.i("Wyślij wiadomość...", "");

        String[] TO = {appContentPreferences.getAppContactMail()};
        //String[] CC = {"lukaszjok@wp.pl"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");


        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        //emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[App_Android] Turek");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Napisz coś... ");

        try {
            startActivity(Intent.createChooser(emailIntent, "Wyślij wiadomość..."));
            //finish();
            Log.i("dbg", "Finished sending email...");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(lukaszjok.com.mturek.GUI.MainActivity.this,
                    R.string.toast_to_email_client_installed, Toast.LENGTH_SHORT).show();
        }
    }

}
