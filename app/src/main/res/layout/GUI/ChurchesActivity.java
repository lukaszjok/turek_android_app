package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.Adapters.AdapterChurchesList;
import lukaszjok.com.mturek.backend.ItemObjects.Church;
import lukaszjok.com.mturek.webservice.RESTapi;

public class ChurchesActivity extends AppCompatActivity {

    private AdView mAdView;
    private AdapterChurchesList churchesListAdapter;
    private ListView lvChurchesList;
    private ArrayList<Church> allChurches;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_churches);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_churches_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillChurchesItems(getApplicationContext());
        lvChurchesList.setAdapter(churchesListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------
    }

    private void initControlsAndVariables(){
        lvChurchesList = findViewById(R.id.lv_churches);
        allChurches = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_churches_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }
    private void initListeners(){
        lvChurchesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int churchId = churchesListAdapter.getItem(position).getIdChurch();

                Intent intent = new Intent(lukaszjok.com.mturek.GUI.ChurchesActivity.this, lukaszjok.com.mturek.GUI.ChurchEventsActivity.class);
                intent.putExtra("church_id", churchId);
                Log.i(DBG_TAG,"In ChurchesActivity, church_id = " + churchId);
                startActivity(intent);
            }
        });

    }


    public void fillChurchesItems(Context ctx) {
        churchesListAdapter = new AdapterChurchesList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_churches", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("churches");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_churches: " + status);
                    Log.i(DBG_TAG,"code_churches: " + code);
                    Log.i(DBG_TAG,"desc_churches: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allChurches.add(new Church(jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("type"),
                                jsonArray.getJSONObject(i).getString("phone"),
                                jsonArray.getJSONObject(i).getString("address")
                        ));
                    }

                    for (int i = 0; i < allChurches.size(); i++) {
                        churchesListAdapter.addItem(allChurches.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvChurchesList.setAdapter(churchesListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, churches)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure restaurants JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillChurchesItems(lukaszjok.com.mturek.GUI.ChurchesActivity.this);
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.ChurchesActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }

}
