package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.Adapters.AdapterHealthList;
import lukaszjok.com.mturek.backend.ItemObjects.Health;
import lukaszjok.com.mturek.webservice.RESTapi;

public class HealthActivity extends AppCompatActivity {
    private AdView mAdView;
    private AdapterHealthList healthListAdapter;
    private ListView lvHealthList;
    private ArrayList<Health> allHealth;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health);
        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_health_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillHealthItems(getApplicationContext());
        lvHealthList.setAdapter(healthListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------
    }

    private void initControlsAndVariables(){
        lvHealthList = findViewById(R.id.lv_health);
        allHealth = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_health_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);


    }
    private void initListeners(){
        lvHealthList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String number = healthListAdapter.getItem(position).getPhone();

                number = number.trim();
                if(number.contains(",")){
                    number = number.substring(0,number.indexOf(","));
                }

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+number));
                startActivity(intent);
            }
        });

    }

    public void fillHealthItems(Context ctx) {
        healthListAdapter = new AdapterHealthList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_health", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("health");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_get_healths: " + status);
                    Log.i(DBG_TAG,"code_get_health: " + code);
                    Log.i(DBG_TAG,"desc_get_health: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allHealth.add(new Health(jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("description"),
                                jsonArray.getJSONObject(i).getString("phone"),
                                jsonArray.getJSONObject(i).getString("work_time"),
                                jsonArray.getJSONObject(i).getString("address")
                        ));
                    }

                    for (int i = 0; i < allHealth.size(); i++) {
                        healthListAdapter.addItem(allHealth.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvHealthList.setAdapter(healthListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, get_health)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure health JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillHealthItems(lukaszjok.com.mturek.GUI.HealthActivity.this);
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.HealthActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }

}