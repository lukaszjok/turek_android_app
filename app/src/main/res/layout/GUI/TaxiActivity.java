package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.Adapters.AdapterTaxiesList;
import lukaszjok.com.mturek.backend.ItemObjects.Taxi;
import lukaszjok.com.mturek.webservice.RESTapi;

public class TaxiActivity extends AppCompatActivity {
    private AdView mAdView;
    private AdapterTaxiesList taxiListAdapter;
    private ListView lvTaxiList;
    private ArrayList<Taxi> allTaxies;
    private ProgressBar pbInitial;

    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taxi);

        initControlsAndVariables();
        initListeners();

        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_taxi_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillTaxiItems(getApplicationContext());
        lvTaxiList.setAdapter(taxiListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------

    }

    private void initControlsAndVariables(){
        lvTaxiList = findViewById(R.id.lv_taxi);
        allTaxies = new ArrayList<>();

        pbInitial = findViewById(R.id.progressBar_taxies_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }
    private void initListeners(){
        lvTaxiList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String number = taxiListAdapter.getItem(position).getPhone();

                number = number.trim();
                if(number.contains(",")){
                    number = number.substring(0,number.indexOf(","));
                }

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+number));
                startActivity(intent);
            }
        });
    }

    public void fillTaxiItems(Context ctx) {
        taxiListAdapter = new AdapterTaxiesList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_taxies", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("taxies");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_taxies: " + status);
                    Log.i(DBG_TAG,"code_taxies: " + code);
                    Log.i(DBG_TAG,"desc_taxies: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allTaxies.add(new Taxi(jsonArray.getJSONObject(i).getString("name"),
                                jsonArray.getJSONObject(i).getString("address"),
                                jsonArray.getJSONObject(i).getString("phone")
                        ));
                    }

                    for (int i = 0; i < allTaxies.size(); i++) {
                        taxiListAdapter.addItem(allTaxies.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvTaxiList.setAdapter(taxiListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, taxies)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure taxies JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillTaxiItems(lukaszjok.com.mturek.GUI.TaxiActivity.this);
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.TaxiActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }

        });

    }
}
