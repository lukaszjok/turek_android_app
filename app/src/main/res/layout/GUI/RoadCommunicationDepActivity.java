package lukaszjok.com.mturek.GUI;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import lukaszjok.com.mturek.R;
import lukaszjok.com.mturek.backend.Adapters.AdapterRoadCommList;
import lukaszjok.com.mturek.backend.ItemObjects.RoadCommQueue;
import lukaszjok.com.mturek.webservice.RESTapi;

public class RoadCommunicationDepActivity extends AppCompatActivity {
    private AdView mAdView;
    private AdapterRoadCommList roadCommListAdapter;
    private SwipeRefreshLayout pullToRefresh;
    private ListView lvRoadCommList;
    private ArrayList<RoadCommQueue> allRoadCommQueues;
    private ProgressBar pbInitial;
    private TextView tvRoadCommListHint;


    private static int failedHttpRequestConuter = 0;
    private static String DBG_TAG = "dbg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_road_communication_dep);
        initControlsAndVariables();
        initListeners();
        // ------ AdMob ------
        MobileAds.initialize(this, getString(R.string.adsense_api_key));
        mAdView = findViewById(R.id.adView_road_comm_activity);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // ------ END AdMob ------

        //--- Contacts City List (adapter) + loading data -----------------
        fillRoadCommItems(getApplicationContext());
        lvRoadCommList.setAdapter(roadCommListAdapter);
        initListeners();
        //--- END Contacts City  (adapter) + loading data -----------------

        //setting an setOnRefreshListener on the SwipeDownLayout
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            int Refreshcounter = 1; //Counting how many times user have refreshed the layout

            @Override
            public void onRefresh() {
                //Here you can update your data from internet or from local SQLite data
                //Refreshcounter = Refreshcounter + 1;

                allRoadCommQueues.clear();
                //--- Contacts City List (adapter) + loading data -----------------
                fillRoadCommItems(getApplicationContext());
                lvRoadCommList.setAdapter(roadCommListAdapter);
                initListeners();
                //--- END Contacts City  (adapter) + loading data -----------------

                pullToRefresh.setRefreshing(false);
            }
        });
    }


    private void initControlsAndVariables(){
        tvRoadCommListHint = findViewById(R.id.tv_road_comm_list_hint);
        pullToRefresh =  findViewById(R.id.pullToRefresh);
        lvRoadCommList = findViewById(R.id.lv_road_comm);
        allRoadCommQueues = new ArrayList<>();
        pbInitial = findViewById(R.id.progressBar_road_communication_activity);
        pbInitial.setVisibility(View.VISIBLE);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        tvRoadCommListHint.setText("Przeciągnij listę ze statusami kolejek w dół, żeby zaktualizować dane.");



    }

    private void initListeners(){

    }

    public void fillRoadCommItems(Context ctx) {
        roadCommListAdapter = new AdapterRoadCommList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("get_road_communication_queues", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("queues");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_road_comm: " + status);
                    Log.i(DBG_TAG,"code_road_comm: " + code);
                    Log.i(DBG_TAG,"desc_road_comm: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allRoadCommQueues.add(new RoadCommQueue(
                                jsonArray.getJSONObject(i).getString("deal_name"),
                                jsonArray.getJSONObject(i).getString("num_of_tickets_in_queue"),
                                jsonArray.getJSONObject(i).getString("current_ticket"),
                                jsonArray.getJSONObject(i).getString("last_ticket")
                        ));
                    }

                    for (int i = 0; i < allRoadCommQueues.size(); i++) {
                        roadCommListAdapter.addItem(allRoadCommQueues.get(i));
                    }
                    pbInitial.setVisibility(View.GONE);
                    lvRoadCommList.setAdapter(roadCommListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, get_road_communication_queues)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure get_road_communication_queues JSON errorResponse " + throwable.getMessage());
                failedHttpRequestConuter++;

                if(failedHttpRequestConuter <3){
                    fillRoadCommItems(lukaszjok.com.mturek.GUI.RoadCommunicationDepActivity.this);
                }else{
                    Toast.makeText(lukaszjok.com.mturek.GUI.RoadCommunicationDepActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestConuter=0;
                }
            }
        });
    }


}
